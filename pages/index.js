import React from 'react'
import Link from 'next/link'
import { Layout, OoyalaPlayer } from '../components';

// TODO: move config to project root
const config = {
  ooyala: {
    apiKey: process.env.OOYALA_API_KEY,
    apiSecret: process.env.OOYALA_API_SECRET,
    brandingId: process.env.OOYALA_BRANDING_ID,
    discoveryId: process.env.OOYALA_DISCOVERY_GID,
    ad: {
      adset: process.env.OOYALA_AD_ADSET,
      host: process.env.OOYALA_AD_HOST,
      baseURL: process.env.OOYALA_AD_BASEURL
    },
    playerVersion: '4.20.8' // released on: 1/28/2018
  }
};

// example video id: A2NGNhZTE6nmSo4QgyIQuMV7cS-PhvCu
// example stream url: http://cf.c.ooyala.com/A2NGNhZTE6nmSo4QgyIQuMV7cS-PhvCu/DOcJ-FxaFrRg4gtDEwOjVncjowczE7v6
const playerProps = {
  ooyalaId: 'A2NGNhZTE6nmSo4QgyIQuMV7cS-PhvCu',
  ooyalaBranding: config.ooyala.brandingId,
  ooyalaPcode: config.ooyala.apiKey.split('.')[0],
  ooyalaPversion: config.ooyala.playerVersion
};

export default () => (
  <Layout title="m2m.tv: sandbox">
    <section>
      <OoyalaPlayer {...playerProps} />
    </section>
    <style global jsx>{`
      section {
        display: flex;
        position: relative;
        min-height: 50vh;
        align-items: center;
        justify-content: center;
        margin-bottom: 100px;
      }
    `}</style>
  </Layout>
)
