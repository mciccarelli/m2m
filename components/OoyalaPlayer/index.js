import React, { Component } from 'react';
import PropTypes from 'prop-types';

let ooyalaScriptsRequested = false;
const ooyalaLoader = [];

/**
 * Adds a player to the script load waiting queue.
 */
const queuePlayer = (player) => ooyalaLoader.push(player);

/**
 * Initializes all of the queued players.
 */
const loadPlayers = (player) => {
  ooyalaLoader.splice(0).forEach((player) => player._createPlayer());
}

/**
 * Loads a list of scripts and fires a callback when done.
 */
const loadScripts = (list, callback) => {
  const waiting = list.slice();
  const loaded = [];

  const load = (src) => {
    const script = document.createElement('script');
    script.src = src;
    script.onload = () => {
      done(src);

      if (waiting.length) {
        load(waiting.shift());
      }
    }
    const firstScript = document.getElementsByTagName('script')[0];
    firstScript.parentNode.insertBefore(script, firstScript);
  };

  const done = (src) => {
    loaded.push(src);
    if (loaded.length === list.length) {
      callback();
    }
  };

  load(waiting.shift());
};

/*
 * @class Displays an Ooyala video
 * @type: Presentational Component
 * @returns OoyalaPlayer
 */
class OoyalaPlayer extends Component {
  constructor(props) {
    super();

    const { ooyalaBranding, ooyalaPcode } = props;
    this._playerParam = {
      'autoplay': true,
      'pcode': ooyalaPcode,
      'playerBrandingId': ooyalaBranding,
      'layout': 'chromeless',
      'skin': {
        'config': '//player.ooyala.com/static/v4/stable/4.20.8/skin-plugin/skin.json'
      }
    }

    this._initOoyalaPlayer.bind(this);
    this._createPlayer.bind(this);
  }

  componentDidMount() {
    const { ooyalaId, ooyalaPversion } = this.props;

    if (!ooyalaScriptsRequested) {
      loadScripts([
        `https://player.ooyala.com/static/v4/stable/${ooyalaPversion}/core.min.js?ver=${ooyalaPversion}`,
        `https://player.ooyala.com/static/v4/stable/${ooyalaPversion}/video-plugin/main_html5.min.js?ver=${ooyalaPversion}`,
        `https://player.ooyala.com/static/v4/stable/${ooyalaPversion}/skin-plugin/html5-skin.min.js?ver=${ooyalaPversion}`,
        `https://player.ooyala.com/static/v4/stable/${ooyalaPversion}/video-plugin/bit_wrapper.min.js?ver=${ooyalaPversion}`,
        // '//player.ooyala.com/core/deb85939b01a45afbe364ba84b09c112'
      ], () => {
        global.OO.ready(loadPlayers);
      });

      ooyalaScriptsRequested = true;
    }

    this._initOoyalaPlayer();
  }

  componentWillUnmount() {
    if (this._player) {
      this._player.destroy();
    }
  }

  _initOoyalaPlayer() {
    const { ooyalaId } = this.props;

    if (!global.OO || !global.OO.Player) {
      queuePlayer(this);
    } else {
      this._createPlayer();
    }
  }

  _createPlayer() {
    if (this._player) return;

    const { ooyalaId } = this.props;

    this._player = global.OO.Player.create(
      `ooyala_player_${ooyalaId}`, ooyalaId, this._playerParam
    );
  }

  render() {
    const { ooyalaId } = this.props;

    return (
      <div className="video-container">
        <figure id={`ooyala_player_${ooyalaId}`} />
        <style jsx>{`
          width: 100%;
          max-width: 800px;
          margin: 0 auto;
          position: relative;

          &:before{
            display: block;
            content: '';
            width: 100%;
            padding-top: (9 / 16) * 100%;
          }

          & figure {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
          }
        `}</style>
        <style global jsx>{`
          body {
            background: whitesmoke;
          }
        `}</style>
      </div>
    );
  };
}

/**
 * propTypes
 * @property {String} ooyalaId - the ID of the video
 * @property {String} ooyalaBranding - Ooyala branding config
 * @property {Strind} ooyalaPcode - Ooyala player code config
 * @property {Strind} ooyalaPversion - Ooyala player version / release
 */
OoyalaPlayer.propTypes = {
  ooyalaId: PropTypes.string.isRequired,
  ooyalaBranding: PropTypes.string.isRequired,
  ooyalaPcode: PropTypes.string.isRequired,
  ooyalaPversion: PropTypes.string.isRequired
};

export default OoyalaPlayer;
