import Link from 'next/link';
import Head from 'next/head';
import { Navigation } from '../';

export default ({ children, title = 'Default Page Title' }) => (
  <div className="app">
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link
        rel="stylesheet"
        href="//cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css"
      />
      <link
        rel="stylesheet"
        href="//player.ooyala.com/static/v4/stable/4.20.8/skin-plugin/html5-skin.min.css"
      />
    </Head>
    <Navigation />
    <main>{children}</main>
    <footer>{'footer content'}</footer>
    <style jsx>{`
      .app {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
        & main {
          flex: 1;
        }
      }
    `}</style>
  </div>
);
