import * as React from 'react';
import Link from 'next/link';

export default () => (
  <header className="navigation">
    <div className="navigation__logo">
      <Link href="/" prefetch>
        <a>{` `}</a>
      </Link>
    </div>
    <nav>
      <ul>
        <li>
          <Link href="/" as="/">
            <a>Ooyala Player</a>
          </Link>
        </li>
        <li>
          <Link href="/b" as="/a">
            <a>Page A</a>
          </Link>
        </li>
        <li>
          <Link href="/a" as="/b">
            <a>Page B</a>
          </Link>
        </li>
      </ul>
    </nav>
    <style jsx>{`
      .navigation,
      .navigation nav > ul {
        display: flex;
      }

      .navigation {
        background: black;
        width: 100%;
        height: 60px;
        position: fixed;
        z-index: 99;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;

        &__logo,
        nav {
          flex: 1;
        }

        &__logo {
          padding-left: 2rem;
          margin: 10px 30px;

          & a {
            display: block;
            width: 36px;
            height: 36px;
            border: 1px solid white;
            background-image: url('../static/m2m-logo.svg');
            background-position: 50% 27%;
            background-size: 36px;
            background-repeat: repeat-y;
            transition: background-position .5s ease-in-out;
            text-indent: -99999px;
            overflow: hidden;

            &:hover {
              background-position: 50% 266%;
            }
          }
        }

        nav ul {
          display: flex;
          justify-content: flex-end;
          & li {
            padding: 0 2rem;
            
          }
          & li a {
            color: white;
            &:hover {
              color: red;
            }
          }
        }
      }
    `}</style>
  </header>
);
