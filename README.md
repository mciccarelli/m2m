# m2m.tv x Ooyala Player Demo

## How to use

Install it and run:

```bash
npm install
npm run dev
```

## Environment Variables

API and other private keys/secrets are stored in an .env file that's untracked by repo.

The following is an example of what a ocal .env should look like:

```bash
OOYALA_API_KEY=xxxx
OOYALA_API_SECRET=xxxx
OOYALA_BRANDING_ID=xxxx
OOYALA_DISCOVERY_GID=xxxx
OOYALA_AD_ADSET=xxxx
OOYALA_AD_HOST=xxxx
OOYALA_AD_BASEURL=xxxx
```

Deploy it to the cloud with [now](https://zeit.co/now) ([download](https://zeit.co/download))

```bash
now
```
